# xenial-build-docker

Docker machine for HiFi builds on Ubuntu Xenial

This was made to support AppImage builds, which require building against an old glibc to be compatible with as many systems as possible.